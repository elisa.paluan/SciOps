ds <- read.csv("../data/raw_ds.csv")

sink("../results/mean_sd.txt")
print("Mean age")
print(mean(ds$Age, na.rm = TRUE))
print("SD age")
print(sd(ds$Age, na.rm = TRUE))
sink()

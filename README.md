# SciOps



## Description:

A project template for using CI/CD, and DevOps concepts, for science.

![](sciops.svg)



## Summary

SciOps' goal is to provide researchers with an easy to use template that
allows them to use cutting-edge computer tooling for next level
productivity, replicability, and transparency in research.

More specifically, SciOps aims to put tools such as `git`, containers
(`Docker`), `R`, `RMarkdown/pandoc`, modern code forge features,
including CI/CD to the hands of researchers in psychology and related
fields.



## Project Status

Currently the prototype template is capable of automatically running `R`
data analysis scripts, in the cloud (automatically installing and
running the required packages in a `Docker` container), as well as
compile an academic paper draft and conference slides using `RMarkdown`.



## Contributing

This project is open to contributions (**under the GPL-3.0-only
license**). Please do not be discouraged of contributing simply because
you're unfamiliar with the computer tools/programs used by SciOps. There
are a lot of non-technical ways you can contribute to the project, for
example, by telling us what features SciOps needs to have for it to be
useful to you.


### GPL-3.0-only

**Please note that, even if specified otherwise in the GitLab UI, this
project is licensed under the GPL-3.0-only.** For more details please
read [the rationale for this
decision](https://gitlab.com/rggd/SciOps/-/commit/3a79052244b3e6a42459eadca5699601de89f5b6).


## Additional Resources

**To learn about SciOps you can:**

- Take a look at [the slides for an upcoming workshop about
  SciOps](https://rggd.gitlab.io/sciops_workshop/slides)

- Take a look at [the slides from a previous workshop on open science
  that mentioned
  SciOps](https://rggd.gitlab.io/sciops_workshop/slides/2022-12-28.html)

- Ask about it in [the RUGGED mailing
  list](mailto:rugged_mailing_list@googlegroups.com)

- Email João O. Santos directly ([joao.filipe.santos@campus.ul.pt](mailto:joao.filipe.santos@campus.ul.pt)).


**To learn about similar concepts, ideas, and projects see the following
references:**

- [Rouder, 2016](https://link.springer.com/article/10.3758/s13428-015-0630-z)

- [Bayser and collaborators, 2015](https://ieeexplore.ieee.org/abstract/document/7140503/references#references)

- [Hupy, 2022](https://about.gitlab.com/blog/2022/02/15/devops-and-the-scientific-process-a-perfect-pairing/)

- [Nüst and collaborators, 2018](https://vickysteeves.gitlab.io/repro-papers/index.html)

- [Brümmer, 2021](https://www.youtube.com/watch?v=4PRFhDIV_4Q)


## Notes

**If you know of other projects with similar goals or other related
references, please, provide them so we can improve this list.**

**If you're working on a related project feel free to reach out so we
can join our efforts if you so choose.**
